from django.apps import AppConfig


class BrianwebsiteappConfig(AppConfig):
    name = 'brian_websiteapp'
